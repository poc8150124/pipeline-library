Object init(String projectName) {
  List<String> splitedJobName = JOB_NAME.split('-')
  int projectVariable = splitedJobName.size()
  String envName = splitedJobName.last()
  if (envName == 'merge' || envName == 'mergedown') {
    envName = 'zeta'
  }
  String envAlias = envName
  String teamJob = ''
  String teamEnv = ''
  String fieldProjectName = projectName
  String projectGitBranch = envName
  String imageEnvironmentTag = envName.capitalize()
  String deployGateWayEnvironment = ''
  String teamName = ''
  if (projectVariable == 3) {
    teamJob = splitedJobName[splitedJobName.size() - 2]
    teamEnv = teamJob + '/dev'
  }
  if (teamEnv != '') {
    teamName = teamEnv.split('/')[0]
    envAlias = teamName
  }
  Map configures = getCIKeyProjectConfigure(envAlias, projectName)
  if (teamJob == '') {
    if (envName == 'alpha') {
      projectGitBranch = '*/dev'
      imageEnvironmentTag = 'Alpha'
      deployGateWayEnvironment = '--stage-name alpha'
        } else if (envName == 'zeta') {
      projectGitBranch = '*/staging'
      imageEnvironmentTag = 'Zeta'
      deployGateWayEnvironment = '--stage-name zeta'
    }
    configures['entrypoint'] = getEntryPoint(configures.project_Path, configures)
  } else {
    if (teamEnv.contains('/dev')) {
      projectGitBranch = teamJob + '/dev'
      imageEnvironmentTag = 'Alpha-' + teamName.capitalize()
      deployGateWayEnvironment = "--stage-name alpha-${teamName.toLowerCase()}"
      String ecsClusterName = configures['ecsClusterName'].replace('{%TEAM%}', teamName).replace('{%ENV%}', envName)
      configures['entrypoint'] = getEntryPoint(configures.project_Path, configures)
      String cloudWatchLogsGroup = configures['cloudWatchLogsGroup'].
                    replace('{%TEAM%}', teamName).replace('{%ENV%}', envName)
      String cloudWatchLogsPrefix = configures['cloudWatchLogsPrefix'].
                    replace('{%TEAM%}', teamName).replace('{%ENV%}', envName)
      configures['ecsClusterName'] = ecsClusterName
      configures['cloudWatchLogsGroup'] = cloudWatchLogsGroup
      configures['cloudWatchLogsPrefix'] = cloudWatchLogsPrefix
    }
  }
  configures.put('imageEnvironmentTag', imageEnvironmentTag)
  configures.put('deployGateWayEnvironment', deployGateWayEnvironment)
  configures.put('projectGitBranch', projectGitBranch)
  configures.put('fieldProjectName', fieldProjectName)
  configures.put('environmentName', envName)
  configures.put('envName', envName)
  return configures
}

Object getCIKeyProjectConfigure(String envName, String projectName) {
  Map responseJson = [:]
  Map configures = [:]
  int pageNumber = 1
  boolean isLastPage = false
  while ({
    try {
      String requestUrl = "http://app-it.ntbx.tech:8281/api/ci-management/project-ci-keys?envName='${envName}'&projectName='${projectName}'&pageNumber='${pageNumber}'"
      responseJson = readJSON text: sh(script: "curl --silent --location --request GET '${requestUrl}'",
                    returnStdout: true).trim()
        } catch (Exception e) {
      print("Cannot Get Response from http://app-it.ntbx.tech:8281/api/ci-management/project-ci-keys?envName='${envName}'&projectName='${projectName}'&pageNumber='${pageNumber}'")
    }
    configures.putAll(responseJson['entities'].collectEntries { configure -> [configure['ciKeySetupConfigure']['key'], configure['value']]
    })
    pageNumber += 1
    isLastPage = responseJson['last']
    !isLastPage
    }()) continue
  return configures
}

void gitCheckout(Map configures) {
  checkout([$class: 'GitSCM', branches: [[name: "${configures['projectGitBranch']}"]],
              doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [],
              userRemoteConfigs: [[credentialsId: '0f1b8964-92e5-481e-bc8c-e5b5ea9caa8c', url: "${configures.project_GitUrl}"]]])
}

void checkoutPrebuild(String sourceBranch, String targetBranch, Map configures) {
  String originBranchName = 'origin'
  try {
    checkout changelog: true, poll: true, scm: [
            $class: "${'GitSCM'}",
            branches: [[name: "${sourceBranch}"]],
            doGenerateSubmoduleConfigurations: false,
            extensions: [[
                $class: 'PreBuildMerge',
                options: [
                    fastForwardMode: 'FF',
                    mergeRemote: "${originBranchName}",
                    mergeStrategy: 'DEFAULT',
                    mergeTarget: "${targetBranch}"
                ]
            ]],
            submoduleCfg: [],
            userRemoteConfigs: [[
                credentialsId: '0f1b8964-92e5-481e-bc8c-e5b5ea9caa8c',
                name: "${originBranchName}",
                url: "${configures.project_GitUrl}"
            ]]
        ]
    } catch (Exception ex) {
    println(ex)
    sh """
            curl -X POST -H 'Authorization: Bearer AYLXfpbjy3sAQk4S0FsJQ3BvyHSUiTboDFIzazuqLW6' \
            -F 'message=[${configures.fieldProjectName.toUpperCase()}] Merge ${sourceBranch}-> \
            ${targetBranch} is Conflict 🔥🔥🔥' https://notify-api.line.me/api/notify
        """
    throw new hudson.AbortException("Checkout ${sourceBranch} to ${targetBranch} is Conflict")
  }
}
Object prepareDocker(Map configures) {
  fieldImageVersion = "${configures.image_RepositoryName}:${configures.image_projectVersion}-${configures.imageEnvironmentTag}"
  subImageVersion = "${configures.image_RepositoryName}${configures.image_RepositoryPath}:${configures.image_projectVersion}-${configures.imageEnvironmentTag}"
  String imagePath = "${configures.image_nexusUrl}/$fieldImageVersion"
  configures.put('fieldImageVersion', fieldImageVersion)
  configures.put('subImageVersion', subImageVersion)
  configures.put('imagePath', imagePath)
  return configures
}

void regressionTest(Map configures) {
  if (configures.regressionTest_Enable != null && configures.regressionTest_Enable == 'true') {
    if (configures.project_Lang == 'python') {
      String projectName = "${configures.image_RepositoryName}".split('/')[1]
      sh"""
        cd ${configures.project_Path}
        docker-compose -f ${configures.project_Path}/docker-compose-ci.yml down --rmi local --remove-orphans
        EXIT_CODE_TEST=\$(docker-compose -f ${configures.project_Path}/docker-compose-ci.yml up --build)
        if [ EXIT_CODE_TEST -eq 1 ] \
        then \
            echo Regression Failure Please Monitor Logs
            docker logs ${projectName}_regression
        fi
      """
    }
  }
}

void cypressAutomatedTesting(Map configures) {
  if (configures['cypress_regressionTest_Enable'] != null && configures['cypress_regressionTest_Enable'] == 'true') {
    try {
      sh """
        docker-compose -f ${configures.project_Path}/docker-compose-automate-ci.yml down --rmi local --remove-orphans
        docker-compose -f ${configures['project_Path']}/docker-compose-automate-ci.yml up --build -d
        cd ${configures['cypress_regressionTest_Path']}
        yarn
        sudo npm run cypress:ci
        docker-compose -f ${env.WORKSPACE}/${configures['project_Path']}/docker-compose-automate-ci.yml down --rmi local --remove-orphans
      """
    } catch (Exception ex) {
      sh"""
        docker logs ${configures['fieldProjectName']}
        docker-compose -f ${env.WORKSPACE}/${configures['project_Path']}/docker-compose-automate-ci.yml down --rmi local --remove-orphans
        deleteDir()
      """
      error(ex)
    }
  }
}

void pushDockerImage(Map configures) {
  pushDockerToECR(configures)
  putLifeCyclePolicy(configures)
}

void pushDockerToECR(Map configures) {
  sh """
    aws ecr describe-repositories --repository-names ${configures.image_RepositoryName} || aws ecr create-repository --repository-name ${configures.image_RepositoryName}
    aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 046321366577.dkr.ecr.ap-southeast-1.amazonaws.com
    docker build -t ${configures.fieldImageVersion} ${configures.project_Path}
    docker tag ${configures.fieldImageVersion} 046321366577.dkr.ecr.ap-southeast-1.amazonaws.com/${configures.fieldImageVersion}
    docker push 046321366577.dkr.ecr.ap-southeast-1.amazonaws.com/${configures.fieldImageVersion}
  """
}

void pushDockerSubImageToECR(Map configures) {
  sh """
    aws ecr describe-repositories --repository-names ${configures.image_RepositoryName}${configures.image_RepositoryPath} || aws ecr create-repository --repository-name ${configures.image_RepositoryName}${'image_RepositaryPath'}
    aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 046321366577.dkr.ecr.ap-southeast-1.amazonaws.com
    docker build -t ${configures['subImageVersion']} ${configures.sub_project_Path}
    docker tag ${configures['subImageVersion']} 046321366577.dkr.ecr.ap-southeast-1.amazonaws.com/${configures['subImageVersion']}
    docker push 046321366577.dkr.ecr.ap-southeast-1.amazonaws.com/${configures['subImageVersion']}
  """
}

void putLifeCyclePolicy(Map configures) {
  String lifeCyclePolicy = libraryResource 'com/ngernturbo/template/life-cycle-policy-rules.json'
  writeFile(file: 'life-cycle-policy-rules.json' ,text: lifeCyclePolicy)

  sh """
    aws ecr put-lifecycle-policy --repository-name ${configures.image_RepositoryName} --lifecycle-policy-text file://life-cycle-policy-rules.json
  """
}

void mergeDown(String sourceBranch, String targetBranch, Map configures) {
  if (configures.mergeDown_Enable != null && configures.mergeDown_Enable == 'true') {
    try {
      sh"""
          echo "==========================[${targetBranch}]=========================="
          rm -rf ./git/${configures.fieldProjectName}/
          git clone ${configures.project_GitUrl} ./git/${configures.fieldProjectName}/
          cd git/${configures.fieldProjectName}/
          git checkout ${sourceBranch}
          git fetch
          git checkout ${targetBranch}
          git merge origin/${sourceBranch}
          git push origin ${targetBranch}
          rm -rf ./git/${configures.fieldProjectName}/
          curl -X POST -H 'Authorization: Bearer AYLXfpbjy3sAQk4S0FsJQ3BvyHSUiTboDFIzazuqLW6' \
          -F 'message=[${configures.fieldProjectName.toUpperCase()}] Merge ${sourceBranch} -> \
          ${targetBranch} is Success ￼￼✅✅✅' https://notify-api.line.me/api/notify
      """
    } catch (Exception ex) {
      sh """
          curl -X POST -H 'Authorization: Bearer AYLXfpbjy3sAQk4S0FsJQ3BvyHSUiTboDFIzazuqLW6' \
          -F 'message=[${configures.fieldProjectName.toUpperCase()}] Merge ${sourceBranch} -> \
          ${targetBranch} is Fail ! 🔥🔥🔥' https://notify-api.line.me/api/notify
        """
    }
  }
}

void unitTest(Map configures) {
  if (configures.unitTest_Enable != null && configures.unitTest_Enable.toBoolean()) {
    sh """
            python3 -m venv unit-test-venv
            source unit-test-venv/bin/activate
            python3 -m pip install --upgrade pip
            python3 -m pip install coverage
            python3 -m pip install --no-cache-dir -r requirements.txt --use-deprecated=legacy-resolver --index-url https://pypi.python.org/simple
            python3 -m coverage erase
            python3 -m coverage run -m unittest discover -v tests
            python3 -m coverage xml -o test-reports/coverage.xml
            deactivate
        """
    junit allowEmptyResults: true, testResults: 'test-reports/*.xml'
  }
}
void addGenerateProjectConfigScriptToProject(Map configures) {
  checkout([$class: 'GitSCM', branches: [[name: '*/master']],
    doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'rogue']], submoduleCfg: [],
    userRemoteConfigs: [[credentialsId: '0f1b8964-92e5-481e-bc8c-e5b5ea9caa8c', url: 'git@git.ntbx.tech:avengers/rogue.git']]])

  sh"""
        cp rogue/rogue.py ${configures.project_Path}/rogue.py
    """
}
void registerLogGroups() {
  String jobDefName = JOB_NAME
  def test = readJSON text:sh( script: "aws logs describe-log-groups --log-group-name-prefix ${jobDefName}-logs", returnStdout: true).trim()
  if (test['logGroups'].size() == 0) {
    sh"""
            aws logs create-log-group --log-group-name ${jobDefName}-logs
        """
  }
  sh"""
        aws logs put-retention-policy --log-group-name ${jobDefName}-logs --retention-in-days 30
    """
}

void registerNewJobDef(Map configures) {
  def envTypes = createEnvTypes()
  String jobName = JOB_NAME
  String projectName = configures['fieldProjectName']
  String defaultEnvironmentVariables = '[{"name":"ENV_NAME","value":"envName"},{"name":"ACTIVE_PROFILE","value":"activeProfile"},{"name":"ASPNETCORE_ENVIRONMENT","value":"envName"},{"name":"GOLANG_ENVIRONMENT","value":"envName"}]'
  defaultEnvironmentVariables = defaultEnvironmentVariables.replace('envName', envTypes['envNameCapitalized'])
  defaultEnvironmentVariables = defaultEnvironmentVariables.replace('activeProfile', envTypes['activeProfile'])
  def projectConfig = getConfigBatchFromLibrary(projectName)
  def jobConfig = projectConfig[projectName]
  List<String> splitedJobName = jobName.split('-')
  List<String> envSplittedName = splitedJobName[1..(splitedJobName.size() - 1)]
  String realEnvName = String.join('-', envSplittedName)
  Map replacement = [
        '%IMAGE_VERSION%':"${configures.image_nexusUrl}/${configures.subImageVersion}",
        '%JOB_DEFINITION_NAME%': "${jobName}",
        '%JOB_ATTEMPTS%':"${jobConfig['jobAttempts']}",
        '%VCPU%':"${jobConfig['cpu']}",
        '%MEMORY%':"${jobConfig['memory']}",
        '%ENTRY_POINT%':"${jobConfig['entryPoint']}",
        '%ENVIRONMENT_VARIABLE%': "${defaultEnvironmentVariables}",
        '%SECRET_PROJECT_CONFIG_ARN%': "${replaceARN(realEnvName)}",
        '%LOGS_GROUP%': "${jobName}-logs"
    ]
  String jobDefTemplateEc2 = libraryResource 'com/ngernturbo/template/job-definition/base-job-definition-dev.json.template'
  String jobDefTemplateFargate = libraryResource 'com/ngernturbo/template/job-definition/base-job-definition-fargate-dev.json.template'
  String jobDefNameFargate = jobName + '-fargate'
  String jobNameFargate = 'ntb-' + projectName + '-' + realEnvName + '-fargate'
  registerJobDefinition(jobDefTemplateEc2, replacement, projectName, jobName)
  registerJobDefinition(jobDefTemplateFargate, replacement, projectName, jobDefNameFargate)
  configures.put('jobDefinitionName', jobDefNameFargate)
  configures.put('jobName', jobNameFargate)
}

void registerJobDefinition(String jobDefTemplate, Map replacement, String projectName, String jobDefName) {
  String completeJobDefinition = replaceStringWithMap(jobDefTemplate, replacement)
  writeFile(file: "${projectName}-job-definition.json" ,text: completeJobDefinition)
  print(readFile("${projectName}-job-definition.json"))
  sh("""
           OLD_JOB_DEF_ARN=`aws batch describe-job-definitions --job-definition-name ${jobDefName} --status ACTIVE | json jobDefinitions[0].jobDefinitionArn`
        echo \$OLD_JOB_DEF_ARN
        if [ -z \$OLD_JOB_DEF_ARN ]
          then aws batch register-job-definition --job-definition-name ${jobDefName} --cli-input-json file://${projectName}-job-definition.json
        fi
    """)
}

def getConfigBatchFromLibrary(String projectName) {
  String requestUrl = "http://thanos.internal-ntb.com:8121/api/devops/config-batch/pipeline?envName=gamma&projectNames=${projectName}"
  def responseJson =  readJSON text:sh( script: "curl --location --request GET '${requestUrl}'",
                        returnStdout: true
                        ).trim()
  return responseJson
}
def replaceStringWithMap(originalString, mapReplacement) {
  def result = originalString
  mapReplacement.each { def entry -> result = result.replaceAll(entry.key, entry.value) }
  return result
}

def createEnvTypes() {
  def result = [:]
  List<String> splittedJobName = JOB_NAME.split('-')
  String activeProfile = splittedJobName[-1].toLowerCase()
  String envNameCapitalized = splittedJobName[-1].capitalize()
  if (splittedJobName.size() > 2) {
    activeProfile = splittedJobName[-1].toLowerCase() + '-' + splittedJobName[1].toLowerCase()
    envNameCapitalized = splittedJobName[-1].capitalize() + splittedJobName[1].capitalize()
  }
  result['activeProfile'] = activeProfile
  result['envNameCapitalized'] = envNameCapitalized
  return result
}

void replaceARN(String realEnvName) {
  responseJson =  readJSON text:sh( script: 'aws secretsmanager describe-secret --secret-id ntb-project-config-' + realEnvName,
            returnStdout: true
            ).trim()
  return responseJson.ARN
}

String getEntryPoint(String projectPath, Map configures) {
  String result = ''
  String appsettingPath = "${projectPath}/${configures['fieldProjectName']}/settings/template.py"
  boolean appsettingPathIsExist = fileExists appsettingPath
  if (appsettingPathIsExist) {
    result = '["bash", "./start.sh"]'
    } else {
    result = configures.project_EntryPoint
  }
  return result
}

void uploadFileToS3(sourcePaths, s3Path) {
  sourcePaths.each { value ->sh "aws s3 sync ${value } s3://${s3Path }/${value}"}
}

void trigerDataSync(serviceName, envName) {
  String taskArn = getTaskArn(serviceName, envName)
  sh("aws datasync start-task-execution --task-arn ${taskArn}")
}

def getTasks() {
  def responseJson =  readJSON text:sh( script: 'aws datasync list-tasks',
                        returnStdout: true
                        ).trim()
  return responseJson
}

String getTaskArn(serviceName, envName) {
  def tasks = getTasks()
  String taskName = serviceName + '-airflow-' + envName
  for (task in tasks['Tasks']) {
    if (taskName.equals(task['Name'])) {
      return task['TaskArn']
    }
  }
}

void submitJobAndWait(Map configures) {
  responseJsonSubmitJob =  readJSON text:sh( script:
      """aws batch submit-job --job-name ${configures['jobName']} --job-queue ${configures['job_queue']} --job-definition ${configures['jobDefinitionName']} --container-overrides command=${configures['project_EntryPoint']}""",
  returnStdout: true
  ).trim()
  String jobId = responseJsonSubmitJob['jobId']
  Integer maxSleepTime = 3600
  Integer durationSleep = 0
  Integer durationSleepStep = 60
  while (durationSleep < maxSleepTime) {
    sleep(durationSleepStep)
    responseJson =  readJSON text:sh( script: "aws batch describe-jobs --jobs ${jobId}",
        returnStdout: true
        ).trim()
    if (responseJson['jobs'][0]['status'] == 'SUCCEEDED') {
      durationSleep += maxSleepTime
      print('MIGRATION BATCH HAS RUN SUCCESSFULLY!!!')
      } else if (responseJson['jobs'][0]['status'] == 'FAILED') {
      error('MIGRATION FAILED!!!')
      } else {
      print('MIGRATION BATCH STILL RUNNING!!!')
      print('STATUS: ' + responseJson['jobs'][0]['status'])
    }
    durationSleep += durationSleepStep
  }
  checkBatchRunSuccess(jobId)
}

void checkBatchRunSuccess(String jobId) {
  responseJson =  readJSON text:sh( script: "aws batch describe-jobs --jobs ${jobId}",
        returnStdout: true
        ).trim()
  if (responseJson['jobs'][0]['status'] != 'SUCCEEDED') {
    error('Failed to migration: TIME-OUT!!!')
  }
}

void forceNewDeploymentECSService(clusterName, serviceName) {
  sh("aws ecs update-service --cluster $clusterName --service $serviceName --force-new-deployment")
}

void updateAirflowServices(clusterName, serviceName, configures) {
  Map latestTaskDefinition = getLatestTaskDefinition(serviceName)
  if (latestTaskDefinition.taskDefinition.containerDefinitions[0].image.equals(configures.imagePath)) {
    forceNewDeploymentECSService(clusterName, serviceName)
  } else {
    registerAirflowTaskDefinition(configures, serviceName, latestTaskDefinition)
    sh("aws ecs update-service --cluster $clusterName --service $serviceName --task-definition $serviceName")
  }
}

Object getLatestTaskDefinition(String serviceName) {
  Map responseJson = [:]
  try {
    responseJson =  readJSON text:sh( script: "aws ecs describe-task-definition --task-definition $serviceName",
                                                        returnStdout: true
                                                        ).trim()
      } catch (Exception e) {
    print("ERROR: cannot describe task definition. Please check the name of this task definition.")
  }
  return responseJson
}

void registerAirflowTaskDefinition(Map configures, String serviceName, Map taskDefinition) {
  taskDefinitionRequest = createNewTaskDefinition(taskDefinition, configures).taskDefinition
  writeFile(file: "$serviceName/task-definition.json" ,text: taskDefinitionRequest.toString())
  print(readFile("$serviceName/task-definition.json"))
  sh("""
        # Register task definition
        aws ecs register-task-definition --cli-input-json file://$serviceName/task-definition.json
    """)
}

Object createNewTaskDefinition(Map responseJson, Map configures) {
  responseJson.taskDefinition.containerDefinitions[0].image = configures['imagePath']
  responseJson.taskDefinition.remove('revision')
  responseJson.taskDefinition.remove('registeredAt')
  responseJson.taskDefinition.remove('registeredBy')
  responseJson.taskDefinition.remove('status')
  responseJson.taskDefinition.remove('requiresAttributes')
  responseJson.taskDefinition.remove('taskDefinitionArn')
  responseJson.taskDefinition.remove('compatibilities')
  return responseJson
}

void checkServiceIsSteadyState(clusterName, serviceName) {
  String eventMessage = ''
  while (!eventMessage.contains('has reached a steady state')) {
    sleep(60)
    def describeService = readJSON text:sh(
      script:"aws ecs describe-services --cluster $clusterName --services $serviceName",
      returnStdout: true
    ).trim()
    if (describeService.services && describeService.services[0].events) {
      eventMessage = describeService.services[0].events[0].message
    }
  }
}

List<String> getServiceNames(Map configures) {
  List<String> serviceNames = []
  response =  readJSON text:sh( script: "aws ecs list-services --cluster ${configures['ecsClusterName']}",
        returnStdout: true
    ).trim()
  print(response['serviceArns'])
  for (serviceArn in response['serviceArns']) {
    serviceName = serviceArn.split('/')[-1]
    serviceNames.add(serviceName)
  }
  return serviceNames
}