Object prepareDocker(Map configures, String version) {
  String fieldImageVersion = "${configures.image_RepositoryName}:v${version}"
  String imagePath = "${configures.image_nexusUrl}/$fieldImageVersion"
  configures.put('fieldImageVersion', fieldImageVersion)
  configures.put('imagePath', imagePath)
  configures.put('imageEnvironmentTag', version)
  return configures
}

void pullImageFromNexus(String version, String projectName) {
    withCredentials([usernamePassword(credentialsId: 'nexus', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        String userPassword = USERNAME + ":" + PASSWORD
        sh """
            wget --user ${USERNAME} --password ${PASSWORD} http://nexus.ntbx.tech/repository/ntb-raw/airflow/${projectName}/${version}/${projectName}-${version}.zip
        """
        sh """
            unzip ${projectName}-${version}.zip -x ".git*"
        """
    }
}