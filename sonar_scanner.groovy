import groovy.transform.Field

@Field Object sonarConfig

Object init(String projectName, Map configures) {
    List<String> splitedJobName = JOB_NAME.split('-')
    String envName = splitedJobName.last()
    int projectVariable = splitedJobName.size()
    if (envName == 'merge' || envName == 'mergedown') {
        envName = 'zeta'
    }
    envName = splitedJobName.last() + '-rep'
    if (configures == null) {
        configures = getCIKeyProjectConfigure(envName, projectName)
    }
    String projectType = ''
    if (configures.vpcEnable != null) {
        projectType = 'LAMBDA'
    }
    configures.put('projectType', projectType)
    return configures
}

Object getCIKeyProjectConfigure(String envName, String projectName) {
  Map responseJson = [:]
  Map configures = [:]
  int pageNumber = 1
  boolean isLastPage = false
  while ({
    try {
      String requestUrl = "http://app-it.ntbx.tech:8281/api/ci-management/project-ci-keys?envName='${envName}'&projectName='${projectName}'&pageNumber='${pageNumber}'"
      responseJson =  readJSON text:sh( script: "curl --silent --location --request GET '${requestUrl}'",
                                                          returnStdout: true
                                                          ).trim()
        } catch (Exception e) {
      print("Cannot Get Response from http://app-it.ntbx.tech:8281/api/ci-management/project-ci-keys?envName='${envName}'&projectName='${projectName}'&pageNumber='${pageNumber}'")
    }
    configures.putAll(
            responseJson['entities'].collectEntries {
            configure -> [configure['ciKeySetupConfigure']['key'], configure['value']]
            }
        )
    pageNumber += 1
    isLastPage = responseJson['last']
    !isLastPage
    }()) continue
  return configures
}

String getEnvironmentName(Boolean alphaTeamName) {
    String result = ''
    String envName = JOB_NAME.split('-')[-1]
    String teamJob = JOB_NAME.split('-')[JOB_NAME.split('-').size() - 2]

    if (teamJob.contains('team') || teamJob.contains('spartan')) {
        if (alphaTeamName == true) {
            result <<= 'alpha-'
        }
        result <<= teamJob
     } else {
        result <<= envName
    }

    return result
}

Object projectToken(String projectName) {
    String projectNameLower = projectName.toLowerCase()
    Map projectTokenData = [:]
    println('Stage: SonarQube projectToken')
    try {
        String envName = getEnvironmentName(false)
        if (envName.contains('team')) {
          projectTokenData = groot_service.getSonarProjectToken(envName, projectNameLower)
          if (projectTokenData.token == null) {
            projectTokenData = groot_service.createSonarProjectToken(envName, projectNameLower)
          }
        } else {
          projectTokenData = hogan_service.getSonarProjectToken(envName, projectNameLower)
          if (envName.matches('^\\w{3}(\\d{2}|_(\\w\\d|\\w{2}))') && projectTokenData.token == null) {
            projectTokenData = hogan_service.getSonarProjectToken('zeta', projectNameLower)
          }
          if (projectTokenData.token == null) {
            hogan_service.createSonarProjectToken(envName, projectNameLower)
            projectTokenData = hogan_service.getSonarProjectToken(envName, projectNameLower)
          }
        }
    } catch (Exception e) {
        println(e)
        unstable('Error Analysis by SonarQube')
    }
  return projectTokenData
}

void projectTypeScanner(String projectName, Map configures) {
    try {
        Map projectTokenData = projectToken(projectName)
        String groot = "http://internal-ntbx-alb-prod-768094285.ap-southeast-1.elb.amazonaws.com:8121/api/devops/projects?name=${projectName.toLowerCase()}"
        String response = sh (
          script:"#!/bin/sh -e\ncurl -X GET --header \"Content-Type: application/json\" ${groot}",
          returnStdout: true
        ).trim()
        Map readObject = readJSON text: response
        Map projectNameReq = readObject.entities[0]
        sonarConfig = getSonarConfig()
        switch (projectNameReq.projectFramework) {
        case 'DOTNET':
                dotnetScanner(projectName, projectTokenData, configures)
                break
        case 'JAVA':
                javaScanner(projectName, projectTokenData, configures)
                break
        case 'NPM':
        case 'YARN':
                nodeJsScanner(projectName, projectTokenData, configures)
                break
        case 'PYTHON':
                pythonScanner(projectNameReq, projectTokenData, configures)
                break
        default:
          otherScanner(projectName, projectTokenData)
                break
        }
  } catch (Exception e) {
        println(e)
        unstable('ERROR SonarQube analysis')
    }
}

void dotnetScanner(String projectNameReq, Map projectTokenData, Map configures) {
    Object sqScannerMsBuildHome = tool 'SonarMSBuild'
    String projectName = projectNameReq.substring(0, 1).toUpperCase() + projectNameReq.substring(1)
    String sonarCoverageExclusions = getSonarCoverageExclusionsDotnet(configures)
    withSonarQubeEnv("sonarqube-${getEnvironmentName(true)}") {
        sh"""#!/bin/sh -e\n
      if [ "${configures.projectType}" = "LAMBDA" ]
      then
        cd ${projectNameReq}
      fi
      dotnet restore --configfile /home/ubuntu/.nuget/NuGet/NuGet.Config
      dotnet ${sqScannerMsBuildHome}/SonarScanner.MSBuild.dll begin /k:\"${projectTokenData.token}\" /n:\"${projectName}\" \
        /d:sonar.cs.opencover.reportsPaths=\"${configures.unitTest_Path}/coverage.opencover.xml\" \
        /d:sonar.exclusions=\"**/wwwroot/**\" \
        /d:sonar.coverage.exclusions=${sonarCoverageExclusions}, \
				/d:sonar.cpd.exclusions=\"${configures.project_Path}/Models/**/*\"
      dotnet build --no-incremental
      dotnet ${sqScannerMsBuildHome}/SonarScanner.MSBuild.dll end
    """
    }
}

void javaScanner(String projectNameReq, Map projectTokenData, Map configures) {
    String projectName = projectNameReq.substring(0, 1).toUpperCase() + projectNameReq.substring(1)
    String pathJava = 'src/main/java/com/ngernturbo/*'
    String pathKt = 'src/main/kotlin/com/ngernturbo/*'
    String sonarCoverageExclusions = getSonarCoverageExclusionsJava()
    withSonarQubeEnv("sonarqube-${getEnvironmentName(true)}") {
        sh"""
      export MAVEN_OPTS=\"-Xmx3072m\"
      if [ "${configures.projectType}" = "LAMBDA" ]
      then
        sh'sudo update-alternatives --set java /usr/lib/jvm/java-11-openjdk-amd64/bin/java'
        cd ${projectNameReq}
      fi
      cd ${configures.project_Path}
      mvn clean install
      mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.6.0.1398:sonar -Dsonar.projectKey=\"${projectTokenData.token}\" \
        -Dsonar.projectName=\"${projectName}\" -Dsonar.junit.reportsPath=\"${configures.project_Path}/target/surefire-reports\" \
        -Dsonar.coverage.exclusions=${sonarCoverageExclusions} \
        -Dsonar.cpd.exclusions=\"**/model/**\"
    """
    }
}

void nodeJsScanner(String projectNameReq, Map projectTokenData, Map configures) {
    Object scannerHome = tool 'SonarQubeScanner'
    String projectName = projectNameReq.substring(0, 1).toUpperCase() + projectNameReq.substring(1)
    withSonarQubeEnv("sonarqube-${getEnvironmentName(true)}") {
        sh """#!/bin/sh -e\n
      if [ "${configures.projectType}" = "LAMBDA" ]
      then
        cd ${projectNameReq}
      fi
      find . -name \"package.json\" -exec rm {} +
      find . -name \".npmrc\" -exec rm {} +
      npm init --force
      yarn add typescript
      ${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=\"${projectTokenData.token}\" -Dsonar.projectName=\"${projectName}\" -Dsonar.coverage.exclusions=\"**/*\" \
        -Dsonar.cpd.exclusions=\"**/*\" -Dsonar.exclusions=\"**/set-webpack-public-path/public-path.js,**/jquery.calendars.picker.js,**/jquery.plugin.js,**/jquery.calendars.plus.js,**/jquery.calendars.js\"
      sudo find . -type d -name \"tests\" -exec rm -rf {} +
    """
    }
}

void pythonScanner(Map projectNameReq, Map projectTokenData, Map configures) {
    Object scannerHome = tool 'SonarQubeScanner'
    String projectReqType = projectNameReq.projectType
    if (projectReqType != 'LAMBDA') {
        String projectName = projectNameReq.name.substring(0, 1).toUpperCase() + projectNameReq.name.substring(1)
        String sonarCoverageExclusions = getSonarCoverageExclusionsPython()
        withSonarQubeEnv("sonarqube-${getEnvironmentName(true)}") {
            sh """
        if [ "${configures.projectType}" = "LAMBDA" ]
        then
          cd ${projectNameReq}
        fi
        #!/bin/sh -e\n${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=\"${projectTokenData.token}\" -Dsonar.projectName=\"${projectName}\" \
          -Dsonar.python.coverage.reportPaths=\"test-reports/coverage.xml\" \
          -Dsonar.coverage.exclusions=${sonarCoverageExclusions} \
          -Dsonar.cpd.exclusions=\"**/test/regression/**\" -Dsonar.exclusions=\"**/model/**,**/migrations/**,**/tests/**,**/unit-test-venv/**,**/__init__.py,**/wiremock/stubs/**,\"
        sudo find . -type d -name \"tests\" -exec rm -rf {} +
      """
        }
  } else {
        sh 'echo PythonLambda Aborted!'
    }
}

void otherScanner(String projectNameReq, Map projectTokenData) {
    Object scannerHome = tool 'SonarQubeScanner'
    String projectName = projectNameReq.substring(0, 1).toUpperCase() + projectNameReq.substring(1)
    withSonarQubeEnv("sonarqube-${getEnvironmentName(true)}") {
        sh "#!/bin/sh -e\n${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=\"${projectTokenData.token}\" -Dsonar.projectName=\"${projectName}\" -Dsonar.coverage.exclusions=\"**/*\""
    }
}

void checkQualityGateByCondition(Map configures) {
    String teamJob = JOB_NAME.split('-')[JOB_NAME.split('-').length - 2]
    boolean isNintendoGroup = configures.project_GitUrl.contains('nintendo')
    if ( !JOB_NAME.contains('spectrum') && !JOB_NAME.contains('saturn') && !JOB_NAME.contains('stafford')) {
        if (configures.projectType != 'LAMBDA' || configures.project_Lang != 'python') {
            checkQualityGate(configures.sonarqube_gate_level)
        }
    }
}

void checkQualityGate(String sonarqubeGateLevel) {
    try {
        timeout(time: 30, unit: 'MINUTES') {
            Object qg = waitForQualityGate()
            if (qg.status != 'OK') {
              if (sonarqubeGateLevel == 'WARNING') {
                unstable("Pipeline error due to quality gate failure: ${qg.status}")
              } else {
                error("Pipeline error due to quality gate failure: ${qg.status}")
              }
            }
        }
    } catch (Exception e) {
        println(e)
        if (sonarqubeGateLevel == 'WARNING') {
          unstable("ERROR SonarQube analysis")
        } else {
          error("ERROR SonarQube analysis")
        }
    }
}

def getSonarConfig() {
  sh"aws s3 cp s3://ntbx-devops/sonar-scanner-config.json ."
  def sonarConfigJson = readJSON file: "sonar-scanner-config.json"
  return sonarConfigJson
}

String getSonarCoverageExclusionsDotnet(def configures) {
  List<String> folderList = sonarConfig.sonar.coverage.exclusions.dotnet
  StringBuilder result = new StringBuilder("\"")
  for (String folder in folderList) {
    result.append("**/${folder}/**/*, ")
  }
  result.append("**/Program.cs, **/Startup.cs\"")
  return result.toString()
}

String getSonarCoverageExclusionsJava() {
  String pathJava = 'src/main/java/com/ngernturbo/*'
  String pathKt = 'src/main/kotlin/com/ngernturbo/*'
  List<String> folderList = sonarConfig.sonar.coverage.exclusions.java
  StringBuilder result = new StringBuilder("\"")
  for (String folder in folderList) {
    result.append("**/${folder}/**, ")
  }
  result.append("${pathJava}/*.java, ${pathKt}/*.kt\"")
  return result.toString()
}

String getSonarCoverageExclusionsPython() {
  String pathJava = 'src/main/java/com/ngernturbo/*'
  String pathKt = 'src/main/kotlin/com/ngernturbo/*'
  List<String> folderList = sonarConfig.sonar.coverage.exclusions.python
  StringBuilder result = new StringBuilder("\"")
  for (String folder in folderList) {
    if (folder != folderList.last()) {
      result.append("**/${folder}/**, ")
    } else {
      result.append("**/${folder}/**\"")
    }
  }
  return result.toString()
}