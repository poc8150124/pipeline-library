import groovy.transform.Field

@Field measureName = new MeasureName()

Object createDimension(String projectName, String environment, String language, String projectType) {
  Map dimension = [:]
  dimension.jobName = JOB_NAME
  dimension.serviceName = projectName
  dimension.environment = environment
  dimension.language = setLanguage(language)
  dimension.projectType = projectType
  return dimension
}

String getEnvironmentName() {
    String result = ''
    String envName = JOB_NAME.split('-')[-1]
    String teamJob = JOB_NAME.split('-')[JOB_NAME.split('-').size() - 2]
    if (teamJob.contains('team') || teamJob.contains('spartan')) {
        result <<= 'alpha-'
        result <<= teamJob
     } else {
        result <<= envName
    }
    return result
}

Object getProjectDetail(String projectName) {
  Map projectDetail = [:]
  String projectToken = getProjectDetailFromGroot(projectName)
  projectTokenData = readJSON text: projectToken
  String language = projectTokenData['entities'][0]['projectFramework']
  String projectType = projectTokenData['entities'][0]['projectType']
  projectDetail.language = setLanguage(language)
  projectDetail.projectType = projectType
  return projectDetail
}

String setLanguage(String language) {
  if (language == 'DOTNET') {
    return 'C#'
  } else if (language == 'NPM' || language == 'YARN') {
    return 'JAVA SCRIPT'
  }
  return language
}

String getProjectDetailFromGroot(String projectName){
  String projectDetailUrl = "http://thanos.ntbx.tech:8121/api/devops/projects?&name=${projectName}"
  def response = httpRequest(httpMode:"GET", url:projectDetailUrl,validResponseCodes:"100:599",contentType:"APPLICATION_JSON")
  return response.content
}

def createMeasure(Map dimension, def records, String measureName, String stageName, long timeDuration, long measureValue) {
  Map measure = [:]
  measure.jobName = dimension.jobName
  measure.serviceName = dimension.serviceName
  measure.environment = dimension.environment
  measure.language = dimension.language
  measure.projectType = dimension.projectType
  measure.measureName = measureName
  measure.stageName = stageName
  measure.measureValue = measureValue
  measure.timeDuration = timeDuration
  records.add(measure)
  return records
} 

void postDeploymentStat(def records) {
  String jsonString = groovy.json.JsonOutput.toJson(records)
  String edithUrl = "http://thanos.ntbx.tech:8278/api/ci-stat/time-stream"

  sh """
    curl --header "Content-Type: application/json" \
      --request POST \
      --data '${jsonString}' \
      ${edithUrl}
  """
}

class MeasureName {
  static final String STAGE_BUILD_DURATION = 'Stage Build Duration'
  static final String BUILD_DURATION = 'Build Duration'
  static final String ERROR = 'Error'
  static final String STAGE_ERROR = 'Stage Error'
  static final String RELEASE_DURATION = 'Release Duration'
  static final String STAGE_RELEASE_DURATION = 'Stage Release Duration'
  static final String MERGEDOWN_DURATION = 'Mergedown Duration'
  static final String STAGE_MERGEDOWN_DURATION = 'Stage Mergedown Duration' 
  static final String STAGE_PRE_BUILD_DURATION = 'Stage Pre Build Duration'
  static final String PRE_BUILD_DURATION = 'Pre Build Duration'
  static final String STAGE_DEPLOY_DURATION = 'Stage Deploy Duration'
  static final String DEPLOY_DURATION = 'Deploy Duration'
  static final String STAGE_ENVIRONMENT_UPDATE_DURATION = 'Stage Environment Update Duration'
  static final String ENVIRONMENT_UPDATE_DURATION = 'Environment Update Duration'
  static final String STAGE_DEPLOY_DEVOPS_SERVICE_DURATION = 'Stage Deploy Devops Service Duration'
  static final String DEPLOY_DEVOPS_SERVICE_DURATION = 'Deploy Devops Service Duration'
  static final String STAGE_CYPRESS_END_TO_END_DURATION = 'Stage Cypress End To End Duration'
  static final String CYPRESS_END_TO_END_DURATION = 'Cypress End To End Duration'
  static final String STAGE_INTEGRATION_TESTS_BY_CYPRESS_DURATION = 'Stage Integration tests By Cypress Duration'
  static final String INTEGRATION_TESTS_BY_CYPRESS_DURATION = 'Integration tests By Cypress Duration'
}
